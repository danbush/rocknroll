<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Heat
 * @since Heat 1.0
 */
?>

	<?php 
		/* enable us to call an arbitrary article */ 
		function showArbitraryPage($path) {
			$post = get_page_by_path($path);
			$content = apply_filters('the_content', $post->post_content);
			echo $content;
		}

		global $featureBlockArticlePath; 
	?> 
	
	<?php if ( isset($featureBlockArticlePath)) : ?>
		<?php if ( $featureBlockArticlePath != '' ) : ?>
			<article id="post-featured" <?php post_class('clearfix featuredBlogPostBlock'); ?>>
				<div class="entry-content-meta-wrapper">
					<div class="entry-content clearfix featuredBlogPostBlock featuredBlogPostContent">
				
						<?php showArbitraryPage($featureBlockArticlePath); ?> 
					
					</div><!-- .entry-content -->
				</div><!-- .entry-content-meta-wrapper -->
			</article><!-- #post-<?php the_ID(); ?> -->
			
			<!-- double-width placeholder container -->
			<article id="post-featured-placeholder" <?php post_class('clearfix featuredBlogPostBlock'); ?>>
				<div class="entry-content-meta-wrapper">
					<div class="entry-content clearfix">
						&nbsp;
					</div><!-- .entry-content -->
				</div><!-- .entry-content-meta-wrapper -->
			</article><!-- #post-<?php the_ID(); ?> -->
			
		<?php endif; ?>
		<?php $featureBlockArticlePath = ''; /* reset the article ID to a null value */ ?> 

	<?php endif; ?>
