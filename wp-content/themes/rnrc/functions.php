<?php
/**
 * RNRC functions and definitions
 * @package WordPress
 * @subpackage rnrc
 */

/* define a shortcode for the music blog filter buttons */ 
add_shortcode( 'musicblogfilters', 'show_musicblog_filters' );
function show_musicblog_filters() {
return <<<SHORTCODE

<div id="filters">
	<input type="radio" id="filters1" name="filterOption" data-filter="*" checked="checked"><label for="filters1">All Posts</label>
	<input type="radio" id="filters2" name="filterOption" data-filter=".featuredBlogPostBlock, .category-featured"><label for="filters2">Featured</label>
	<input type="radio" id="filters3" name="filterOption" data-filter=".featuredBlogPostBlock, .category-most-shared"><label for="filters3">Most Shared</label>
</div>
			
<script>
	jQuery(document).ready(function ($) {
		$( "#filters" ).buttonset();
	});
</script>

SHORTCODE;
}

/* define a shortcode for the social blog filter buttons */ 
add_shortcode( 'socialblogfilters', 'show_socialblog_filters' );
function show_socialblog_filters() {
return <<<SHORTCODE

<div id="filters">
	<input type="radio" id="filters1" name="filterOption" data-filter="*" checked="checked"><label for="filters1">All Posts</label>
	<input type="radio" id="filters2" name="filterOption" data-filter=".featuredBlogPostBlock, .category-featured"><label for="filters2">Featured</label>
	<input type="radio" id="filters3" name="filterOption" data-filter=".featuredBlogPostBlock, .category-most-shared"><label for="filters3">Most Shared</label>
</div>

<script>
	jQuery(document).ready(function ($) {
		$( "#filters" ).buttonset();
	});
</script>

SHORTCODE;
}



/* cause royalSlider to clear its caches when articles are updated, so that featured items sliders for blog posts will update properly */ 
function rs_clear_cache() { 
	global $wpdb;
	$sql = "
		delete from {$wpdb->options}
		where option_name like '_transient_new-royalslider-posts-%' or option_name like '_transient_timeout_new-royalslider-posts-%'
		";
	return $wpdb->query($sql);	
} 
add_action( 'save_post', 'rs_clear_cache' );


